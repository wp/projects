package mk.ukim.finki.wp.projects.repository;

import mk.ukim.finki.wp.projects.model.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, String> {
    List<Professor> findAllByName(String name);

    Optional<Professor> findById(String s);
}
