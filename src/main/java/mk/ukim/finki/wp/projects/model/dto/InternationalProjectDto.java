package mk.ukim.finki.wp.projects.model.dto;

import lombok.Data;
import mk.ukim.finki.wp.projects.model.enums.TypeStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class InternationalProjectDto {

    private String name;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate dateEntry;
    private TypeStatus typeStatus;
    private String description;
    private String type;
    private String goals;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate startDate;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate endDate;
    private Long primaryInstitution;
    private Long anotherInstitution;
    private Long carrier;
    private Long partners;


    public InternationalProjectDto(String name, LocalDate dateEntry, TypeStatus typeStatus, String description, String type, String goals, LocalDate startDate, LocalDate endDate, Long primaryInstitution, Long anotherInstitution, Long carrier, Long partners) {
        this.name = name;
        this.dateEntry = dateEntry;
        this.typeStatus = typeStatus;
        this.description = description;
        this.type = type;
        this.goals = goals;
        this.startDate = startDate;
        this.endDate = endDate;
        this.primaryInstitution = primaryInstitution;
        this.anotherInstitution = anotherInstitution;
        this.carrier = carrier;
        this.partners = partners;
    }

}
