package mk.ukim.finki.wp.projects.repository;

import mk.ukim.finki.wp.projects.model.ScientificProject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScientificProjectRepository extends JpaRepository<ScientificProject, Long> {

}
