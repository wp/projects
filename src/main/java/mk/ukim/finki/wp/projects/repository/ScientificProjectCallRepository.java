package mk.ukim.finki.wp.projects.repository;

import mk.ukim.finki.wp.projects.model.ScientificProjectCall;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScientificProjectCallRepository extends JpaRepository<ScientificProjectCall, Long> {

}
