package mk.ukim.finki.wp.projects.model.dto;

import lombok.Data;

@Data
public class GrantHolderDto {
    String name;
    String description;

    public GrantHolderDto(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
