package mk.ukim.finki.wp.projects.model.dto;

import lombok.Data;
import mk.ukim.finki.wp.projects.model.enums.TypeStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Data
public class NationalProjectDto {
    private String name;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate dateEntry;
    private TypeStatus typeStatus;
    private String keyWords;
    private String summary;
    private String benefits;
    private List<String> members;
    private String manager;
    private Long callId;


    public NationalProjectDto(String name, LocalDate dateEntry, TypeStatus typeStatus, String keyWords, String summary, String benefits, List<String> members, String manager, Long callId) {
        this.name = name;
        this.dateEntry = dateEntry;
        this.typeStatus = typeStatus;
        this.keyWords = keyWords;
        this.summary = summary;
        this.benefits = benefits;
        this.members = members;
        this.manager = manager;
        this.callId = callId;
    }
}
