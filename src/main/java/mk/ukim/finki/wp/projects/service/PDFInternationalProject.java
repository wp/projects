package mk.ukim.finki.wp.projects.service;

import com.lowagie.text.DocumentException;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

public interface PDFInternationalProject {
    //TODO: the service layer must not contain request and response. Return some kind of object that will represent the pdf!
    public void exportInternationalProject(HttpServletResponse response,
                                           Long internationalProjectId) throws DocumentException, IOException;

    public void internationProjectsReport(HttpServletResponse response) throws DocumentException, IOException;
}
