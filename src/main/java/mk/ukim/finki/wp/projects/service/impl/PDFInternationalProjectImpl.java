package mk.ukim.finki.wp.projects.service.impl;

import com.lowagie.text.Font;
import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfWriter;
import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.wp.projects.service.PDFInternationalProject;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PDFInternationalProjectImpl implements PDFInternationalProject {


    @Override
    public void exportInternationalProject(HttpServletResponse response, Long internationalProjectId) throws DocumentException, IOException {
        Document document=new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();

        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(20);
        font.setColor(Color.BLACK);

        Paragraph heading = new Paragraph("Faculty of Computer Science and Engineering - International Project", font);
        heading.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(heading);
        Paragraph blank = new Paragraph(" ");
        document.add(blank);

        Font font1 = FontFactory.getFont(FontFactory.HELVETICA);
        font1.setSize(16);
        font1.setColor(Color.BLACK);

        Optional internationalProject = null; //internationalProjectService.findById(internationalProjectId);

        if(internationalProject.isPresent())
        {
//            Paragraph name = new Paragraph("Name : " + internationalProject.get().getName(), font1);
//            name.setAlignment(Paragraph.ALIGN_LEFT);
//            document.add(name);
//            document.add(blank);
//
//            Paragraph dateEntry = new Paragraph("Date of entry : " + internationalProject.get().getDateEntry(), font1);
//            dateEntry.setAlignment(Paragraph.ALIGN_LEFT);
//            document.add(dateEntry);
//            document.add(blank);

//            Paragraph status = new Paragraph("Status : " + internationalProject.get().getTypeStatus().toString(), font1);
//            status.setAlignment(Paragraph.ALIGN_LEFT);
//            document.add(status);
//            document.add(blank);

//            Paragraph description = new Paragraph("Description : " + internationalProject.get().getDescription(), font1);
//            description.setAlignment(Paragraph.ALIGN_LEFT);
//            document.add(description);
//            document.add(blank);
//
//            Paragraph goals = new Paragraph("Goals : " + internationalProject.get().getGoals(), font1);
//            goals.setAlignment(Paragraph.ALIGN_LEFT);
//            document.add(goals);
//            document.add(blank);
//
//            Paragraph primaryInstitution = new Paragraph("Primary Grant Holder : " + internationalProject.get().getPrimaryGrantHolder().getName(), font1);
//            primaryInstitution.setAlignment(Paragraph.ALIGN_LEFT);
//            document.add(primaryInstitution);
//            document.add(blank);
        }
        document.close();
    }

    @Override
    public void internationProjectsReport(HttpServletResponse response) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();

        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(20);
        font.setColor(Color.BLACK);

        Paragraph heading = new Paragraph("Faculty of Computer Science and Engineering - International Project Report", font);
        heading.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(heading);
        Paragraph blank = new Paragraph(" ");
        document.add(blank);

        Font font1 = FontFactory.getFont(FontFactory.HELVETICA);
        font1.setSize(16);
        font1.setColor(Color.BLACK);

        List internationalProjects = new ArrayList();

//        for (InternationalProject internationalProject : internationalProjects) {
//            Paragraph name = new Paragraph("Name : " + internationalProject.getName(), font1);
//            name.setAlignment(Paragraph.ALIGN_LEFT);
//            document.add(name);
//            document.add(blank);
//        }

        document.close();
    }
}
