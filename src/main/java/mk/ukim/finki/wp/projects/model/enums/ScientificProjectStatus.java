package mk.ukim.finki.wp.projects.model.enums;

public enum ScientificProjectStatus {

    DRAFT, SUBMITTED, ACCEPTED, REJECTED, ACTIVE, PENDING_REPORT, ACCOMPLISHED, FAILED
}
