package mk.ukim.finki.wp.projects.service;

import mk.ukim.finki.wp.projects.model.ScientificProjectCall;
import mk.ukim.finki.wp.projects.model.dto.AddScientificProjectCallDto;
import mk.ukim.finki.wp.projects.model.dto.ScientificProjectCallDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ScientificProjectCallService {
    List<ScientificProjectCall> findAll();

    Optional<ScientificProjectCall> findById(Long id);

    void delete(Long id);

    Optional<ScientificProjectCall> addCall(AddScientificProjectCallDto scientificProjectCallDto);

    Optional<ScientificProjectCall> editCall(Long id, ScientificProjectCallDto scientificProjectCallDto);

    Page<ScientificProjectCall> findAllByPagination(Pageable pageable);
}
